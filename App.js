import React from 'react';
import { Provider, } from 'react-redux';
import { createAppContainer } from "react-navigation";
import store from './src/store/configStore';
import { setTopLevelNavigator } from './src/actions/navigation';

import ScreensList from './Screens';

console.disableYellowBox = true

const AppContainer = createAppContainer(ScreensList);

export default () => (
  <Provider store={store}>
    <AppContainer ref={navigatorRef => { setTopLevelNavigator(navigatorRef) }} />
  </Provider>
)