import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrap: {
    backgroundColor: '#000',
    flex: 1,
  },
  btnWrap: {
    width: '100%',
  },
  btn: {
    backgroundColor: 'rgb(242,52,50)',
    paddingVertical: 14,
  },
  btnDisable: {
    opacity: 0.6,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  btnFail: {
    backgroundColor: 'rgb(152,37,37)',
  },
  btnWin: {
    backgroundColor: 'rgb(83,165,61)',
  },
  btnText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '600',
    alignSelf: 'center',
  },
});