import {
  LOGIN_USER_SUCCESS,
  CHECK_SUB_SUCCESS,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  SET_USER_PROP,
  USER_LOGOUT
} from '../actions/actionsTypes';

const inital = {
  auth: false,
  displayName: null,
  email: null,
  emailVerified: false,
  isAnonymous: false,
  metadata: {
    creationTime: null,
    lastSignInTime: null
  },
  phoneNumber: null,
  photoURL: null,
  providerData: [],
  providerId: "firebase",
  refreshToken: null,
  uid: null,
  premium: false,
};

const userReducer = (state = inital, action) => {
  switch (action.type) {
    case SET_USER_PROP:
    case FETCH_USER_SUCCESS:
    case LOGIN_USER_SUCCESS:
    case CHECK_SUB_SUCCESS:
      return {
        ...state,
        ...action.payload,
      }
    case FETCH_USER_FAILURE:
    case USER_LOGOUT:
      return {
        ...inital,
      }

    default:
      return state
  }
};

export default userReducer;