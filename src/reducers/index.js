import { combineReducers } from 'redux';

import userReducer from './user';
import navigationReducer from './navigation';
import loginReducer from './login';
import betsReducer from './bets';
import bookmakersReducer from './bookmakers';

export default combineReducers({
  user: userReducer,
  navigation: navigationReducer,
  login: loginReducer,
  bets: betsReducer,
  bookmakers: bookmakersReducer,
});