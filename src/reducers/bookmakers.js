import {
  FETCH_BOOKMAKERS_LIST_SUCCESS
} from '../actions/actionsTypes';

const inital = [];

const bookmakersReducer = (state = inital, action) => {
  switch (action.type) {
    case FETCH_BOOKMAKERS_LIST_SUCCESS:
      return action.payload
    default:
      return state
  }
};

export default bookmakersReducer;