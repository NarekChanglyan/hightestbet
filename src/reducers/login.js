import {
  CHANGE_LOGIN_PROP
} from '../actions/actionsTypes';

const inital = {
  phone: '',
  code: '',
  step: 0,
};

const loginReducer = (state = inital, action) => {
  switch (action.type) {
    case CHANGE_LOGIN_PROP:
      return {
        ...state,
        ...action.payload,
      }
    default:
      return state
  }
};

export default loginReducer;