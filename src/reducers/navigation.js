import {
  CHANGE_STATE_NAVIGATION,
  CLOSE_BOOKMAKERS,
  OPEN_BOOKMAKERS,
} from '../actions/actionsTypes';

const inital = {
  page: 'MainScreen',
  bookmakers: false,
};

const navigationReducer = (state = inital, action) => {
  switch (action.type) {
    case CHANGE_STATE_NAVIGATION:
      return {
        ...state,
        page:action.page,
      }
    case OPEN_BOOKMAKERS:
      return {
        ...state,
        bookmakers:true,
      }
    case CLOSE_BOOKMAKERS:
      return {
        ...state,
        bookmakers:false,
      }

    default:
      return state
  }
};

export default navigationReducer;