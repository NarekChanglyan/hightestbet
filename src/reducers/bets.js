import {
  FETCH_BETS_LIST_SUCCESS
} from '../actions/actionsTypes';

const inital = [];

const betsReducer = (state = inital, action) => {
  switch (action.type) {
    case FETCH_BETS_LIST_SUCCESS:
      return action.payload
    default:
      return state
  }
};

export default betsReducer;