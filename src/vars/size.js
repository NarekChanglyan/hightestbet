import { Dimensions } from 'react-native';

export const deviceWidthSize = Dimensions.get('window').width;
export const deviceHeightSize = Dimensions.get('window').height;

export const isXS = () => (deviceWidthSize > 374 && deviceHeightSize > 811);
export const isFive = () => (deviceWidthSize < 321 && deviceHeightSize < 569);
export const isSix = () => (deviceWidthSize < 376 && deviceHeightSize < 668);
export const isSixPlus = () => (deviceWidthSize < 415 && deviceHeightSize < 737);

export const topMargin = isXS() ? 41 : 21;

export const onBoardingIcoSize = () => {
  let size = { width: '100%' };

  if(isFive()) {
    size = {
      width: 160,
      height: 160,
    }
  } else {
    size = {
      width: 200,
      height: 200,
    }
  }

  return size;
};
