import React, { memo } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import auth, { firebase } from '@react-native-firebase/auth';
import moment from 'moment';
import FastImage from 'react-native-fast-image';

import Header from '../../components/Header';
import { signOutUser } from '../../actions/user';

import mS from '../../mainStyle';
import s from './ProfilePageStyle';
import out from './images/out.png';

const ProfilePage = ({ user, navigation, signOutUser }) => (
  <View style={mS.wrap}>
    <Header navigation={navigation} />

    <View style={s.profile}>
      <View style={s.contentWrap}>
        <View style={s.content}>
          <View style={s.leftBlock}>
            <Text style={s.leftBlockItem}>Телефон:</Text>
            <Text style={s.leftBlockItem}>Подписка:</Text>
            {user.premium && <Text style={s.leftBlockItem}>Истекает:</Text>}
          </View>
          <View style={s.rightBlock}>
            <Text style={s.rightBlockItem}>{user.phoneNumber && user.phoneNumber}</Text>

            <Text style={s.rightBlockItem}>{user.premium ? 'Premium' : 'Неактивна'}</Text>
            {
              user.premium && <Text style={s.rightBlockItem}>{user.premiumExpires && moment(user.premiumExpires).format('DD.MM.YYYY')}</Text>
            }

          </View>
        </View>
      </View>

      <View style={s.logoutWrap}>
        <TouchableOpacity
          style={s.logout}
          onPress={async () => {
            await signOutUser();
            await auth().signOut();
            navigation.navigate('RegScreen')
          }}
          activeOpacity={0.9}
        >
          <FastImage
            style={s.logoutIco}
            source={out}
            resizeMode={FastImage.resizeMode.contain}
          />
          <Text style={s.logoutText}>ВЫЙТИ ИЗ ПРОФИЛЯ</Text>
        </TouchableOpacity>
      </View>
    </View>

  </View>
);

export default connect(
  ({ user }) => ({ user }),
  dispatch => bindActionCreators({ signOutUser }, dispatch)
)(memo(ProfilePage));