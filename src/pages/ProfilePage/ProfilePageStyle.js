import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrap: {

  },
  profile: {
    paddingHorizontal: 18,
  },
  contentWrap: {
    paddingHorizontal: 22,
    paddingTop: 20,
    backgroundColor: 'rgb(56,61,67)',
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  rightBlock: {
    paddingLeft: 24,
  },
  leftBlockItem: {
    color: 'rgb(155,158,161)',
    paddingBottom: 18,
    fontSize: 14,
  },
  rightBlockItem: {
    paddingBottom: 18,
    color: '#fff',
    fontSize: 14,
    fontWeight: '600'
  },
  logoutWrap: {
    borderTopColor: 'rgba(255,255,255, .1)',
    borderTopWidth: 1,
    backgroundColor: 'rgb(56,61,67)'
  },
  logout: {
    paddingLeft: 22,
    paddingVertical: 18,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  logoutText: {
    fontSize: 14,
    color: 'rgba(255,255,255,.5)'
  },
  logoutIco: {
    marginRight: 7,
    width: 18,
    height: 18,
  },
  title: {
    fontSize: 16,
    color: '#fff',
    paddingBottom: 20,
  },
});