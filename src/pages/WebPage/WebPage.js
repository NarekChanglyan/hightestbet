import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { WebView } from 'react-native-webview';

import Header from '../../components/Header';

import s from './WebPageStyle';


export default class WebPage extends React.Component {
  state = {
    loading: false,
  }

  componentWillMount() {
    this.subs = [
      this.props.navigation.addListener('didFocus', () => {
        const url = this.props.navigation.getParam('url', 'null');;

        if (url === 'null') this.props.navigation.navigate('MainScreen');
      }),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  render() {
    const { loading } = this.state;
    const url = this.props.navigation.getParam('url', 'null');;
    
    return (
      <View style={s.wrap}>
        <Header navigation={this.props.navigation} style={{ zIndex: 9999, backgroundColor: 'rgb(254,54,52)', height: 100 }} />

        {
          loading && (
            <View style={s.loading}>
              <ActivityIndicator color={'rgb(242,52,50)'} size={'small'} />
            </View>
          )
        }

        <WebView
          key={`WebViewPage${url}`}
          style={s.webView}
          source={{uri:url}}
          onLoadStart={() => this.setState({ loading: true })}
          onLoadEnd={() => this.setState({ loading: false })}
          javaScriptEnabled={true}

        />
      </View>
    );
  }
};