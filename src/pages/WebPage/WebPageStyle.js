import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrap: {
    backgroundColor: 'red',
    flex: 1,
  },
  webView: {
    width: '100%',
    height: '100%',
  },
  loading: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 999,
    backgroundColor: 'rgba(0,0,0,.5)'
  }
});