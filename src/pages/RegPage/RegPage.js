import React from 'react';
import { View, TouchableOpacity, Text, Alert, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TextInputMask } from 'react-native-masked-text';
import FastImage from 'react-native-fast-image';
import auth, { firebase } from '@react-native-firebase/auth';

import Header from '../../components/Header';
import { changeLoginProp } from '../../actions/login';
import { setUserProps, checkSubscribe } from '../../actions/user';
import { fetchBetsList, fetchBookmakersList } from '../../actions/bets';
import check from './images/check.png';
import mS from '../../mainStyle';
import s from './RegPageStyle';

class RegPage extends React.Component {
  state = {
    validPhone: false,
    validCode: false,
    loading: false,
  }

  confirmResult = null

  componentWillMount() {
    this.subs = [
      this.props.navigation.addListener('didFocus', () => {
        this.clearForm();
      }),
    ];
  }

  componentWillUnmount() {
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  clearForm = () => {
    this.props.changeLoginProp('step', 0);
    this.props.changeLoginProp('phone', '');
    this.props.changeLoginProp('code', '');
    this.setState({ validPhone: false, validCode: false });
  }

  sendCode = async () => {
    this.setState({
      loading: true,
    }, async () => {
      const { phone } = this.props.login;

      try {
        const confirm = await auth().signInWithPhoneNumber(phone);
        this.confirmResult = confirm;

        this.props.changeLoginProp('step', 1);
      } catch (err) {
        console.log(err)
      }

      this.setState({ loading: false })
    });
  }

  checkCode = async () => {
    const { code } = this.props.login;

    this.setState({
      loading: true,
    }, async () => {
      try {
        const res = await this.confirmResult.confirm(code);
        const idTokenResult = await firebase.auth().currentUser.getIdTokenResult(true);

        Alert.alert('Успешно', 'Добро пожаловать в HightestBET!');
        const { _user } = res;

        await this.props.setUserProps({
          auth: true,
          token: idTokenResult.token,
          ..._user,
        });

        await this.props.checkSubscribe();
        await this.props.fetchBetsList();
        await this.props.fetchBookmakersList();

        this.props.navigation.navigate('MainScreen');
      } catch (err) {
        Alert.alert('Ошибка', 'Ввели неправильный код подтверждения!')
        console.log(err)
      }

      this.setState({ loading: false })
    });
  }


  render() {
    const { step, phone, code } = this.props.login;
    const { loading, validPhone, validCode } = this.state;

    return (
      <View style={mS.wrap}>
        <Header navigation={this.props.navigation} />

        <View style={s.wrap}>

          <View style={s.content}>
            {
              !!step ? (
                <View>
                  <View style={s.desc}>
                    <Text style={s.descText}>Для регистрации в сервисе введите код подтверждения, отправленный вам по СМС на указанный номер</Text>
                  </View>

                  <View>
                    <TextInputMask
                      type={'custom'}
                      options={{
                        mask: '999999',
                        validator: (value) => value.search(/^[0-9]{6,6}$/i) > -1,
                      }}
                      keyboardType={'phone-pad'}
                      placeholder={'Код'}
                      style={s.textInput}
                      value={code}
                      editable={!loading}
                      onChangeText={async (text) => {
                        await this.props.changeLoginProp('code', text);

                        if (this.smscode.isValid()) this.setState({ validCode: true });
                        else this.setState({ validCode: false });
                      }}
                      ref={(ref) => this.smscode = ref}
                    />
                    {
                      validCode && (
                        <FastImage
                          style={s.check}
                          source={check}
                          resizeMode={FastImage.resizeMode.contain}
                        />
                      )
                    }
                  </View>
                  <View style={mS.btnWrap}>
                    <TouchableOpacity
                      onPress={() => {
                        if (validCode) this.checkCode();
                      }}
                      activeOpacity={0.9}
                      style={loading ? { ...mS.btn, ...mS.btnDisable } : mS.btn}
                      disabled={loading}
                    >
                      {loading && <ActivityIndicator color={'#fff'} size={'small'} style={{ marginRight: 8 }} />}
                      <Text style={mS.btnText}>ВОЙТИ</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={s.btnsWrap}>
                    <TouchableOpacity
                      style={s.btn}
                      onPress={() => {
                        this.clearForm();
                        this.props.changeLoginProp('step', 0);
                      }}
                      activeOpacity={0.9}
                    >
                      <Text style={s.btnText}>Отмена</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      style={s.btn}
                      onPress={() => {
                        this.clearForm();
                        this.props.changeLoginProp('step', 0);
                      }}
                      activeOpacity={0.9}
                    >
                      <Text style={{ ...s.btnText, color: 'rgb(251,100,98)' }}>Отправить еще раз</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              ) : (
                  <View>
                    <View>
                      <TextInputMask
                        type={'custom'}
                        options={{
                          mask: '+7 999-999-9999',
                          validator: (value) => value.replace(/[ ()-]/g, '').search(/^\+[1-9]{1}[0-9]{10,14}$/i) > -1,
                        }}
                        keyboardType={'phone-pad'}
                        placeholder={'Номер телефона'}
                        style={s.textInput}
                        value={phone}
                        editable={!loading}
                        onChangeText={async (text) => {
                          await this.props.changeLoginProp('phone', text);

                          if (this.phoneField.isValid()) this.setState({ validPhone: true });
                          else this.setState({ validPhone: false });
                        }}
                        ref={(ref) => this.phoneField = ref}
                      />
                      {
                        validPhone && (
                          <FastImage
                            style={s.check}
                            source={check}
                            resizeMode={FastImage.resizeMode.contain}
                          />
                        )
                      }
                    </View>
                    <View style={mS.btnWrap}>
                      <TouchableOpacity
                        onPress={async () => {

                          if (validPhone) {
                            await this.sendCode();
                          }
                        }}
                        disabled={loading}
                        activeOpacity={0.9}
                        style={loading ? { ...mS.btn, ...mS.btnDisable } : mS.btn}
                      >
                        {loading && <ActivityIndicator color={'#fff'} size={'small'} style={{ marginRight: 8 }} />}
                        <Text style={mS.btnText}>ОТПРАВИТЬ КОД</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                )
            }

          </View>
        </View>
      </View>
    );
  }
};

export default connect(
  ({ login }) => ({ login }),
  dispatch => bindActionCreators({ changeLoginProp, setUserProps, checkSubscribe, fetchBetsList, fetchBookmakersList }, dispatch),
)(RegPage);