import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    width: '100%',
    paddingHorizontal: 18,
    marginTop: -140,
  },
  textInput: {
    backgroundColor: '#fff',
    color: '#000',
    width: '100%',
    height: 65,
    fontSize: 22,
    textAlign: 'center',
    marginBottom: 25,
  },
  check: {
    position: 'absolute',
    left: 22,
    top: 24,
    zIndex: 99,
    width: 26,
    height: 16,
  },
  btnsWrap: {
    marginTop: 27,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btnText: {
    fontSize: 15,
    fontWeight: '600',
    color: '#fff'
  },
  desc: {
    marginBottom: 36,
  },
  descText: {
    color: 'rgba(255,255,255,.5)',
    fontSize: 13,
    fontWeight: '400',
    textAlign: 'center'
  },
});