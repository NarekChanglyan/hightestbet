import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import auth, { firebase } from '@react-native-firebase/auth';

import Header from '../../components/Header';
import MatchList from '../../components/MatchList';
import BookmakersModal from '../../components/BookmakersModal';

import mS from '../../mainStyle';
import s from './MainPageStyle';

import { fetchBetsList, fetchBookmakersList } from '../../actions/bets';
import { setUserProps, checkSubscribe } from '../../actions/user';

class MainPage extends React.Component {
  async componentWillMount() {
    firebase.auth().onAuthStateChanged((res) => {
      if (res && res._user) {
        this.props.setUserProps({
          auth: true,
          ...res._user,
        });
      }
    });
    const userAuth = await firebase.auth().currentUser;
    if (userAuth) {
      const idTokenResult = await firebase.auth().currentUser.getIdTokenResult(true);
      await this.props.setUserProps({ token: idTokenResult.token })

      await this.props.checkSubscribe();
    }

    await this.props.fetchBetsList();
    await this.props.fetchBookmakersList();
  }

  render() {
    return (
      <View style={mS.wrap}>
        <Header navigation={this.props.navigation} />

        <MatchList />

        <BookmakersModal />
      </View>
    );
  }
}

export default connect(
  ({ user }) => ({ user }),
  dispatch => bindActionCreators({ fetchBetsList, fetchBookmakersList, setUserProps, checkSubscribe }, dispatch)
)(MainPage);