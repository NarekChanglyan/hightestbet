import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { WebView } from 'react-native-webview';

import Header from '../../components/Header';
import { orderPayment, checkSubscribe } from '../../actions/user'
import { fetchBetsList, fetchBookmakersList } from '../../actions/bets'
import s from './StorePageStyle';

class StorePage extends React.Component {
  state = {
    loading: false,
  }

  handleNavigationChange = async (res) => {
    const { url } = res;

    if (url.indexOf('#token') > -1) {
      this.setState({
        loading: true,
      }, async () => {
        const token = this.getToken(url);

        try {
          await this.props.orderPayment(token, 1, 'EUR');

          await this.props.checkSubscribe();
          await this.props.fetchBetsList();
          await this.props.fetchBookmakersList();
          this.props.navigation.navigate('MainScreen');

        } catch (err) {
          this.setState({ loading: false });
          console.log(err)
        }
      })
    }
  }

  getToken(url) {
    const hash = url.split('#');
    const token = hash.find(item => item.indexOf('token') > -1).replace('token=', '');

    return token;
  }

  render() {
    const { loading } = this.state;

    return (
      <View style={s.wrap}>
        <Header navigation={this.props.navigation} style={{ zIndex: 9999, backgroundColor: 'rgb(254,54,52)', height: 100 }} />

        {
          loading && (
            <View style={s.loading}>
              <ActivityIndicator color={'rgb(242,52,50)'} size={'small'} />
            </View>
          )
        }

        <WebView
          style={s.webView}
          source={{uri:'https://index.toprognoz.ru/'}}
          onNavigationStateChange={this.handleNavigationChange}
          onLoadStart={() => this.setState({ loading: true })}
          onLoadEnd={() => this.setState({ loading: false })}
          javaScriptEnabled={true}
        />
      </View>
    );
  }
};

export default connect(
  ({ user }) => ({ user }),
  dispatch => bindActionCreators({ orderPayment, checkSubscribe, fetchBetsList, fetchBookmakersList }, dispatch),
)(StorePage);