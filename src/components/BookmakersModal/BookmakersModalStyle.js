import { StyleSheet } from 'react-native';
import { isXS } from '../../vars/size';

export default StyleSheet.create({
  wrap: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
  },
  bg: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0,0,0,.85)'
  },
  titleWrap: {
    width: '100%',
    paddingTop: isXS() ? 60 : 46,
    paddingHorizontal: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 24,
    color: '#fff',
    fontWeight: '600',
  },
  content: {
    marginTop: 10,
  },
  closeWrap: {
    width: 50,
    height: 50,
  },
  close: {
    width: 30,
    height: 30,
    alignSelf: 'flex-end'
  },
});