import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { branch, renderComponent, renderNothing } from 'recompose';
import { bindActionCreators } from 'redux';
import FastImage from 'react-native-fast-image';

import BookmakersList from '../BookmakersList';
import { closeBookmakers } from '../../actions/navigation';

import close from './images/close.png';
import s from './BookmakersModalStyle';

class BookmakersModal extends React.Component {
  render() {
    return (
      <View style={s.wrap}>
        <TouchableOpacity
          style={s.bg}
          activeOpacity={0.9}
          onPress={() => { this.props.closeBookmakers(); }}
        />

        <View style={s.titleWrap}>
          <Text style={s.title}>Сделать ставку</Text>

          <TouchableOpacity
            onPress={() => { this.props.closeBookmakers() }}
            activeOpacity={0.9}
            style={s.closeWrap}
          >
            <FastImage
              source={close}
              style={s.close}
              resizeMode={FastImage.resizeMode.contain}
            />
          </TouchableOpacity>
        </View>

        <View style={s.content}>
          <BookmakersList />
        </View>
      </View>
    );
  }
}

export default connect(
  ({ navigation }) => ({ navigation }),
  dispatch => bindActionCreators({ closeBookmakers }, dispatch)
)(branch(
  ({ navigation }) => (navigation.bookmakers),
  renderComponent(BookmakersModal),
  renderNothing
)(BookmakersModal));