import React from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import FastImage from 'react-native-fast-image';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HTML from 'react-native-render-html';
import moment from 'moment';

import { openBookmakers, navigate } from '../../actions/navigation';
import mS from '../../mainStyle';
import s from './MatchComponentStyle';

const DEFAULT_HTML_PROPS = {
  renderers: {
    p: (k, children) => <Text key={`MatchCompText${Math.random()}`} style={s.descItem}>{children}</Text>,
    b: (k, children) => <Text key={`MatchCompTextKey${Math.random()}`} style={s.descItemKey}>{children}</Text>,
    br: () => <Text key={`MatchCompBr${Math.random()}`} style={s.descItem} />,
    textwrapper: () => <Text key={`MatchCompText${Math.random()}`} style={s.descItem} />,
  },
}

class MatchComponent extends React.Component {
  render() {
    const { navigate, openBookmakers, time, content, image_url, id, is_paid, user } = this.props;
    const { auth, premium } = user;

    return (
      <View style={s.wrap}>
        <View style={s.title}>
          <Text style={s.countdown}></Text>
          <Text style={s.date}>{time && moment(time).format('DD.MM.YYYY hh:mm')}</Text>
        </View>


        {
          !!image_url && (
            <View style={s.coverWrap}>
              <FastImage
                style={s.cover}
                source={{ uri: image_url }}
                resizeMode={FastImage.resizeMode.cover}
              />
            </View>
          )
        }
        
        {
          (is_paid && (!auth || !premium)) && (
            <View style={s.desc}>
              <View style={s.storeDesc}>
                <Text style={s.storeDescText}>Доступно только владельцам подписки</Text>
              </View>
            </View>
          )
        }
        {
          (!is_paid || (auth && premium)) && (
            <View style={s.desc}>
              {
                !!content && (
                  <HTML
                    {...DEFAULT_HTML_PROPS}
                    key={`MatchCompHTML${id}`}
                    html={content}
                    imagesMaxWidth={Dimensions.get('window').width}
                  />
                )
              }
            </View>
          )
        }

        <View style={mS.btnWrap}>
          {
            (is_paid && (!auth || !premium)) ? (
              <TouchableOpacity
                onPress={() => {
                  if (!auth) {
                    navigate('RegScreen');
                  } else {
                    navigate('StoreScreen')
                  }
                }}
                activeOpacity={0.9}
                style={mS.btn}
              >
                <Text style={mS.btnText}>КУПИТЬ ПОДПИСКУ ЗА 1 990 РУБ.</Text>
              </TouchableOpacity>

            ) : (
                <TouchableOpacity
                  onPress={() => { openBookmakers() }}
                  activeOpacity={0.9}
                  style={mS.btn}
                >
                  <Text style={mS.btnText}>СДЕЛАТЬ СТАВКУ</Text>
                </TouchableOpacity>

              )
          }
          {/* <View style={{ ...mS.btn, ...mS.btnWin }}>
            <Text style={mS.btnText}>СТАВКА ПРОШЛА</Text>
          </View>
          <View style={{ ...mS.btn, ...mS.btnFail }}>
            <Text style={mS.btnText}>СТАВКА НЕ ПРОШЛА</Text>
          </View> */}
        </View>
      </View>
    )
  }
}

export default connect(
  ({ user }) => ({ user }),
  dispatch => bindActionCreators({ openBookmakers, navigate }, dispatch),
)(MatchComponent);