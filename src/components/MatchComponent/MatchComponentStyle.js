import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrap: {
    backgroundColor: 'rgba(56, 61, 67, 1)',
    marginBottom: 24
  },
  title: {
    paddingVertical: 13,
    paddingHorizontal: 13,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  countdown: {
    fontSize: 12,
    fontWeight: '600',
    color: '#fff'
  },
  date: {
    fontSize: 12,
    fontWeight: '600',
    color: 'rgba(255,255,255,.5)'
  },
  coverWrap: {
    height: 190,
    width: '100%'
  },
  cover: {
    height: '100%',
    width: '100%',
  },
  desc: {
    paddingVertical: 13,
    paddingHorizontal: 13,
  },
  storeDesc: {
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  storeDescText: {
    color: 'rgba(255,255,255,.5)',
    textAlign: 'center'
  },
  descItem: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '400',
    marginBottom: 8,
  },
  descItemKey: {
    fontWeight: '700',
    color: '#fff',
  },
  htmlView: {
    width: '100%',
    height: 100,
  },
});