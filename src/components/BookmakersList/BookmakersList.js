import React, { memo } from 'react';
import { View, ScrollView } from 'react-native';
import { connect } from 'react-redux';

import Bookmaker from '../Bookmaker';

import s from './BookmakersListStyle';

const BookmakersList = ({ bookmakers }) => (
  <View style={s.wrap}>
    <ScrollView
      horizontal={false}
      contentContainerStyle={s.listWrap}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
    >
      {
        bookmakers.map((item, index) => <Bookmaker key={`BookmakerComp${index}${item.title}`} {...item} />)
      }
    </ScrollView>
  </View>
);

export default connect(
  ({ bookmakers }) => ({ bookmakers }),
  null
)(memo(BookmakersList));