import { StyleSheet } from 'react-native';
import { isXS } from '../../vars/size';

export default StyleSheet.create({
  wrap: {
    width: '100%',
  },
  bg: {
    height: 360,
    marginBottom: isXS() ? -240 : -260,
  },
  logo: {
    width: 130,
    height: 40,
  },
  version: {
    position: 'absolute',
    left: 18,
    top:  isXS() ? 90 : 76,
    fontSize: 12,
    color: 'rgba(255,255,255,.4)'
  },
  content: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    paddingTop: isXS() ? 50 : 36,
    paddingHorizontal: 18,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  user: {
    marginTop: -5,
    height: 60,
    width: 120,
    alignSelf: 'center',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row'
  },
  userIco: {
    height: 30,
    width: 30,
  },
  premiumWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  premium: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '500',
    marginRight: 9,
    marginLeft: 4,
  },
  check: {
    marginTop: -2,
    width: 26,
    height: 16,
  },
  bookmakerWrap: {
    alignSelf: 'flex-start',
    borderColor: 'rgba(255, 255, 255, .1)',
    borderWidth: 1,
    paddingHorizontal: 18,
    paddingTop: 10,
    paddingBottom: 9,
  },
  bookmaker: {
    fontWeight: '600',
    fontSize: 18,
    color: '#fff',
  },
  bookmakerActiveWrap: {
    alignSelf: 'flex-start',
    backgroundColor: '#fff',
    paddingHorizontal: 18,
    paddingTop: 10,
    paddingBottom: 9,
  },
  bookmakerActive: {
    fontWeight: '600',
    fontSize: 18,
    color: 'rgb(242, 52, 50)',
  },
  backBtn: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
  },
  backBtnText: {
    color: '#fff',
    fontSize: 29,
    fontWeight: '600'
  },
  backBtnIc: {
    marginRight: 11,
    height: 40,
    width: 30,
  },
  title: {
    fontSize: 24,
    color: '#fff',
    fontWeight: '600',
  },
  closeWrap: {
    marginTop: -15,
    height: 60,
    width: 60,
    alignSelf: 'center',
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
  },
  close: {
    width: 30,
    height: 30,
  },
});