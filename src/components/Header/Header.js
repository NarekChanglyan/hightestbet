import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import FastImage from 'react-native-fast-image';

import s from './HeaderStyle';
import logo from './images/logo.png';
import check from './images/check.png';
import close from './images/close.png';
import userIco from './images/user.png';
import bg from './images/bg.png';

const MainHeader = ({ user, navigation }) => (
  <View style={s.wrap}>
    <FastImage
      style={s.bg}
      source={bg}
      resizeMode={FastImage.resizeMode.stretch}
    />

    <View style={s.content}>
      <FastImage
        style={s.logo}
        source={logo}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Text style={s.version}>v1.0</Text>

      <TouchableOpacity
        activeOpacity={1}
        onPress={() => { 
          navigation.navigate(user.auth ? 'ProfileScreen' : 'RegScreen')
        }}
        style={s.user}
      >
        {
          user.premium && (
            <View style={s.premiumWrap}>
              <FastImage
                source={check}
                style={s.check}
                resizeMode={FastImage.resizeMode.contain}
                />
              <Text style={s.premium}>Premium</Text>
            </View>
          )
        }

        <FastImage
          source={userIco}
          style={s.userIco}
          resizeMode={FastImage.resizeMode.contain}
        />
      </TouchableOpacity>
    </View>
  </View>
);

const ProfileHeader = ({ navigation }) => (
  <View style={s.wrap}>
    <FastImage
      style={s.bg}
      source={bg}
      resizeMode={FastImage.resizeMode.stretch}
    />

    <View style={{...s.content, marginTop: 10}}>
      <Text style={s.title}>Профиль</Text>

      <TouchableOpacity
        onPress={() => { navigation.navigate('MainScreen') }}
        activeOpacity={0.9}
        style={s.closeWrap}
      >
        <FastImage
          source={close}
          style={s.close}
          resizeMode={FastImage.resizeMode.contain}
        />
      </TouchableOpacity>
    </View>
  </View>
);

const RegHeader = ({ navigation }) => (
  <View style={s.wrap}>
    <FastImage
      style={{...s.bg, opacity: 0}}
      source={bg}
      resizeMode={FastImage.resizeMode.stretch}
    />

    <View style={{...s.content, marginTop: 10}}>
      <Text style={s.title}>Регистрация и вход</Text>

      <TouchableOpacity
        onPress={() => { navigation.navigate('MainScreen') }}
        activeOpacity={0.9}
        style={s.closeWrap}
      >
        <FastImage
          source={close}
          style={s.close}
          resizeMode={FastImage.resizeMode.contain}
        />
      </TouchableOpacity>
    </View>
  </View>
);

const StoreHeader = ({ navigation, style }) => (
  <View style={{...s.wrap, ...style}}>
    <View style={{...s.content, marginTop: 10}}>
      <Text style={s.title}>Подписка</Text>

      <TouchableOpacity
        onPress={() => { navigation.navigate('MainScreen') }}
        activeOpacity={0.9}
        style={s.closeWrap}
      >
        <FastImage
          source={close}
          style={s.close}
          resizeMode={FastImage.resizeMode.contain}
        />
      </TouchableOpacity>
    </View>
  </View>
);

const WebHeader = ({ navigation, style }) => (
  <View style={{...s.wrap, ...style}}>
    <View style={{...s.content, marginTop: 10}}>
      <Text style={{...s.title, fontSize: 20}}>Сделать ставку</Text>

      <TouchableOpacity
        onPress={() => { navigation.navigate('MainScreen') }}
        activeOpacity={0.9}
        style={s.closeWrap}
      >
        <FastImage
          source={close}
          style={s.close}
          resizeMode={FastImage.resizeMode.contain}
        />
      </TouchableOpacity>
    </View>
  </View>
);

const Header = (props) => {
  const page = props.navigation.state.routeName;
  
  if(page === 'MainScreen') return (<MainHeader {...props} />);
  if(page === 'RegScreen') return (<RegHeader {...props} />);
  if(page === 'ProfileScreen') return (<ProfileHeader {...props} />);
  if(page === 'StoreScreen') return (<StoreHeader {...props} />);
  if(page === 'WebScreen') return (<WebHeader {...props} />);
};

export default connect(
  ({ user }) => ({ user }),
  null
)(Header);