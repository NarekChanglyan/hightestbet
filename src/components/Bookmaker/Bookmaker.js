
import React, { memo } from 'react';
import { View, Text, TouchableOpacity, Linking } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FastImage from 'react-native-fast-image';

import { closeBookmakers, navigate } from '../../actions/navigation';

import mS from '../../mainStyle';
import s from './BookmakerStyle';

const Bookmaker = ({ title, image_url, url, description, navigate }) => (
  <View style={s.wrap}>
    <View style={s.titleWrap}>
      <View style={s.title}>
        {
          image_url && (
            <FastImage
              style={s.titleIco}
              source={{ uri: image_url }}
              resizeMode={FastImage.resizeMode.contain}
            />
          )
        }
        <Text style={s.titleText}>{title && title}</Text>
      </View>

      <Text style={s.benefit} />
    </View>

    {
      description && (
        <View style={s.desc}>
          <Text style={s.descText}>{description}</Text>
        </View>
      )
    }

    <View style={mS.btnWrap}>
      <TouchableOpacity
        onPress={() => { navigate('WebScreen', {url}) }}
        style={mS.btn}
        activeOpacity={0.9}
      >
        <Text style={mS.btnText}>СДЕЛАТЬ СТАВКУ</Text>
      </TouchableOpacity>
    </View>
  </View>
);

export default connect(
  null,
  dispatch => bindActionCreators({ closeBookmakers, navigate }, dispatch)
)(memo(Bookmaker));