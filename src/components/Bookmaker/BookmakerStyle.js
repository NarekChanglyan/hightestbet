import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrap: {
    paddingBottom: 26,
  },
  titleWrap: {
    paddingHorizontal: 13,
    paddingVertical: 13,
    backgroundColor: 'rgb(56,61,67)',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  titleIco: {
    width: 27,
    height: 27,
    borderRadius: 30,
    marginRight: 9,
  },
  titleText: {
    fontSize: 14,
    color: '#fff',
    fontWeight: '500',
  },
  benefit: {
    fontSize: 14,
    color: '#fff',
    fontWeight: '600'
  },
  desc: {
    paddingHorizontal: 13,
    paddingVertical: 13,
    backgroundColor: 'rgb(69,76,83)'
  },
  descText: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '500',
  },
});