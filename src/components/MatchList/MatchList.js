import React from 'react';
import { View, ScrollView, ActivityIndicator } from 'react-native';
import { connect } from "react-redux";

import s from './MatchListStyle';

import MatchComponent from '../MatchComponent';

const MatchList = ({ bets }) => (
  <View style={{flex:1}}>
    {
      bets.length === 0 ? (
        <ActivityIndicator size={'large'} color={'#fff'} />
      ) : (
          <ScrollView
            horizontal={false}
            contentContainerStyle={s.wrap}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            key={`MatchListWrap${bets.length}`}
          >
            {
              bets.map((item, index) => (<MatchComponent
                key={`MatchComponentKey${index}${item.id}`}
                {...item}
              />))
            }
          </ScrollView >
        )
    }
  </View>
);

export default connect(
  ({ bets }) => ({ bets }),
  null
)(MatchList);