import {
  CHANGE_STATE_NAVIGATION,
  CLOSE_BOOKMAKERS,
  OPEN_BOOKMAKERS,
} from './actionsTypes';
import { NavigationActions } from 'react-navigation';

let _navigator;

export const setTopLevelNavigator = (navigatorRef) => {
  _navigator = navigatorRef;
}

export const openBookmakers = () => (dispatch) => {

  dispatch({
    type: OPEN_BOOKMAKERS,
  });
};

export const closeBookmakers = () => (dispatch) => {

  dispatch({
    type: CLOSE_BOOKMAKERS,
  });
};

export const navigate = (routeName, params) => (dispatch) => {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );

  dispatch({
    type: CHANGE_STATE_NAVIGATION,
    page: routeName,
  });
};

export const navigateWithoutDispatch = (dispatch, routeName, params) => {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );

  dispatch({
    type: CHANGE_STATE_NAVIGATION,
    page: routeName,
  });
};