import {
	CHANGE_LOGIN_PROP
} from './actionsTypes';

export const changeLoginProp = (key, val) => async (dispatch) => {
	const payload = {};
	payload[key] = val;

	dispatch({
		type: CHANGE_LOGIN_PROP,
		payload,
	})
}