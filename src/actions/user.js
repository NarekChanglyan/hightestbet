import {
	LOGIN_USER_START,
	LOGIN_USER_SUCCESS,
	LOGIN_USER_FAILURE,
	SET_USER_PROP,
	USER_LOGOUT,
	ORDER_PAYMENT_START,
	ORDER_PAYMENT_SUCCESS,
	ORDER_PAYMENT_FAILURE,
	CHECK_SUB_START,
	CHECK_SUB_SUCCESS,
	CHECK_SUB_FAILURE
} from './actionsTypes';

import {
	signin,
	order,
	checkSubscribe as checkSubscribeApi,
} from '../api/user';

export const login = (vkToken) => async (dispatch) => {
	dispatch({ type: LOGIN_USER_START })

	try {
		const user = await signin(vkToken);
		const token = await user.data.token;

		if (user.success) {
			dispatch({
				type: LOGIN_USER_SUCCESS,
				payload: { token },
			});
		} else {
			dispatch({
				type: LOGIN_USER_FAILURE,
				payload: user.status,
			})
		}
	} catch (err) {
		dispatch({
			type: LOGIN_USER_FAILURE,
			payload: err,
		})
	}
}

export const setUserProps = (val) => async (dispatch) => {
	dispatch({
		type: SET_USER_PROP,
		payload: val,
	})
}

export const signOutUser = (val) => async (dispatch) => {
	dispatch({
		type: USER_LOGOUT,
	})
}

export const setUserProp = (key, val) => async (dispatch) => {
	const payload = {};
	payload[key] = val;

	dispatch({
		type: SET_USER_PROP,
		payload,
	})
}

export const orderPayment = (paymentToken, amount, currency) => async (dispatch, getState) => {
	dispatch({ type: ORDER_PAYMENT_START })

	const state = getState();
	const { user } = state;

	return new Promise(async (resolve, reject) => {
		try {
			const resOrder = await order(user.token, paymentToken, amount, currency);

			if (resOrder.success) {

				dispatch({
					type: ORDER_PAYMENT_SUCCESS,
				});

				resolve(resOrder.success);
			} else {
				dispatch({
					type: ORDER_PAYMENT_FAILURE,
					payload: resOrder.code,
				});

				reject(resOrder.code);
			}
		} catch (err) {
			console.log(err)
			dispatch({
				type: ORDER_PAYMENT_FAILURE,
				payload: err,
			});

			resolve(err);
		}
	})
}

const hasSub = (subInfo) => {
	const { status, updated } = subInfo;
	const now = new Date().getTime();
	const updateDate = new Date(updated.timestampValue);
	const expiresDate = updateDate.setDate(updateDate.getDate() + 30);

	return (status.stringValue==='success' && now < expiresDate) ? expiresDate : false;
}

export const checkSubscribe = () => async (dispatch, getState) => {
	const state = getState();
	const { user } = state;
	const { auth, token, uid } = user;

	dispatch({ type: CHECK_SUB_START });

	if (auth) {
		try {
			const res = await checkSubscribeApi(token, uid);
			const expiresDate = hasSub(res.documents[0].fields);

			if (res.documents && res.documents[0] && expiresDate) {
				dispatch({
					type: CHECK_SUB_SUCCESS,
					payload: {
						premiumExpires: expiresDate,
						premium: true,
					}
				});
			} else {
				dispatch({
					type: CHECK_SUB_SUCCESS,
					payload: {
						premium: false
					}
				});
			}
		} catch (err) {
			dispatch({
				type: CHECK_SUB_FAILURE,
				payload: err,
			});
		}
	} else {
		dispatch({
			type: CHECK_SUB_FAILURE,
			payload: 'User unauthorized'
		});
	}
};