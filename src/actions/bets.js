import {
  FETCH_BETS_LIST_START,
  FETCH_BETS_LIST_SUCCESS,
  FETCH_BETS_LIST_FAILURE,
  FETCH_BOOKMAKERS_LIST_START,
  FETCH_BOOKMAKERS_LIST_SUCCESS,
  FETCH_BOOKMAKERS_LIST_FAILURE,
} from './actionsTypes';

import {
  fetchBetsList as fetchBetsListApi,
  fetchBookmakersList as fetchBookmakersListApi,
} from '../api/bets';

export const fetchBetsList = () => async (dispatch, getState) => {
  const state = getState();

  dispatch({ type: FETCH_BETS_LIST_START });

  try {
    const betsList = await fetchBetsListApi(state.user.token);

    if (betsList.success) {
      dispatch({
        type: FETCH_BETS_LIST_SUCCESS,
        payload: betsList.data.posts,
      });
    } else {
      dispatch({
        type: FETCH_BETS_LIST_FAILURE,
        payload: betsList.code,
      });
    }

  } catch (err) {
    console.log(err);
  }
}

export const fetchBookmakersList = () => async (dispatch) => {
  dispatch({ type: FETCH_BOOKMAKERS_LIST_START });

  try {
    const bookmakersList = await fetchBookmakersListApi();

    if (bookmakersList.success) {
      dispatch({
        type: FETCH_BOOKMAKERS_LIST_SUCCESS,
        payload: bookmakersList.data.bookmakers,
      });
    } else {
      dispatch({
        type: FETCH_BOOKMAKERS_LIST_FAILURE,
        payload: bookmakersList.code,
      });
    }

  } catch (err) {
    console.log(err);
  }
}