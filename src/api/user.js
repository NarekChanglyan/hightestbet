import { apiUrl } from '../vars/config';
import axios from 'axios';

export const signin = async (vkToken) => {
  const res = await axios.get(`${apiUrl}/auth?provider=vk&token=${vkToken}`);
  const user = res.data;

  return user;
};

export const order = async (userToken, paymentToken, amount, currency) => {
  const res = await axios.post(`${apiUrl}/orders`, {
    'token': paymentToken,
    'amount': amount,
    'currency': currency
  }, {
      headers: {
        'Content-Type': 'text/json',
        'X-Auth-Token': userToken,
      },
    });

  return res.data;
}

export const checkSubscribe = async (userToken, usdrId) => {
  const res = await axios.get(`https://firestore.googleapis.com/v1/projects/highestbet-test/databases/(default)/documents/users/${usdrId}/orders/`, {
    headers: {
      'Authorization': 'Bearer ' + userToken
    },
  });

  return res.data;
}
