import axios from 'axios';
import { apiUrl } from '../vars/config';

export const fetchBetsList = async (userToken) => {
  const headers = !!userToken ? { headers: { 'X-Auth-Token': userToken } } : null;
  const res = await axios.get(`${apiUrl}/posts`, headers);

  return res.data;
};

export const fetchBookmakersList = async () => {
  const res = await axios.get(`${apiUrl}/bookmakers`);

  return res.data;
};