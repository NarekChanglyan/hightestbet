import React from 'react';
import { createStackNavigator } from 'react-navigation';

import MainPage from './src/pages/MainPage';
import ProfilePage from './src/pages/ProfilePage';
import RegPage from './src/pages/RegPage';
import StorePage from './src/pages/StorePage';
import WebPage from './src/pages/WebPage';

const headerStyles = {
  header: null,
};

const AppScreen = createStackNavigator({
  MainScreen: {
    screen: MainPage,
    navigationOptions: headerStyles,
  },
  ProfileScreen: {
    screen: ProfilePage,
    navigationOptions: headerStyles,
  },
  RegScreen: {
    screen: RegPage,
    navigationOptions: headerStyles,
  },
  StoreScreen: {
    screen: StorePage,
    navigationOptions: headerStyles,
  },
  WebScreen: {
    screen: WebPage,
    navigationOptions: headerStyles,
  },
}, {
    initialRouteName: 'MainScreen',
    transitionConfig: () => ({
      screenInterpolator: ({ position, scene }) => {
        const { index } = scene;

        const opacity = position.interpolate({
          inputRange: [index - 1, index],
          outputRange: [0, 1],
        });

        return { opacity };
      },
    })
  });


export default AppScreen;
